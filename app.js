const express = require('express');
const app = express();

const morgan = require('morgan');


const func = require('functions');


app.use(morgan('dev'))


const members = [

	{
		id: 1,
		name: "Gibojan"
	},
	{
		id: 2,
		name: "Eric Dampierre"
	}
]



app.get("/api/v1/members/:id", (req, res) => {
	res.json(func.success(members[(req.params.id)-1].name))
})


app.get("/api/v1/members", (req, res) => {
	res.json(func.success(members))
})





app.listen(8080, () => console.log('app started on port 8080'))